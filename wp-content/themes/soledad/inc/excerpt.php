<?php
add_filter( 'penci_the_excerpt', 'wptexturize' );
add_filter( 'penci_the_excerpt', 'convert_smilies' );
add_filter( 'penci_the_excerpt', 'convert_chars' );
add_filter( 'penci_the_excerpt', 'wpautop' );
add_filter( 'penci_the_excerpt', 'shortcode_unautop' );

/**
 * Display the post excerpt.
 */
if ( ! function_exists( 'penci_the_excerpt' ) ):
	function penci_the_excerpt( $length = 25 ) {
		echo apply_filters( 'penci_the_excerpt', penci_get_the_excerpt( null, $length ) );
	}
endif;
/**
 * Retrieves the post excerpt.
 */
if ( ! function_exists( 'penci_get_the_excerpt' ) ):
	function penci_get_the_excerpt( $post = null, $length = 30 ) {
		$post = get_post( $post );
		if ( empty( $post ) ) {
			return '';
		}

		if ( post_password_required( $post ) ) {
			return __( 'There is no excerpt because this is a protected post.' );
		}

		return  penci_trim_excerpt( $post->post_excerpt, $length );
	}
endif;

/**
 * Generates an excerpt from the content, if needed.
 *
 * The excerpt word amount will be 30 words and if the amount is greater than
 * that, then the string ' ...' will be appended to the excerpt. If the string
 * is less than 30 words, then the content will be returned as is.

 * @param string $text Optional. The excerpt. If set to empty, an excerpt is generated.
 *
 * @return string The excerpt.
 */
if ( ! function_exists( 'penci_trim_excerpt' ) ):
function penci_trim_excerpt( $text = '', $length = 30 ) {
	$raw_excerpt = $text;
	if ( '' == $text ) {
		$text = get_the_content( '' );

		$text = strip_shortcodes( $text );
		$text = function_exists( 'excerpt_remove_blocks' ) ? excerpt_remove_blocks( $text ) : $text;
		$text = apply_filters( 'the_content', $text );
		$text = str_replace( ']]>', ']]&gt;', $text );
	}

	if ( '' == $text ) {
		return '';
	}

	if( ! $length ) {
		$length = 30;
	}

	$text  = wp_trim_words( $text, $length, ' ...' );
	return $text;
}
endif;